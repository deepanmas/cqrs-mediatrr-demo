﻿using eCommerce.ApplicationServices.Commands;
using eCommerce.ApplicationServices.Notifications;
using eCommerce.ApplicationServices.Queries;
using eCommerce.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace eCommerce.Controllers
{
    public class ProductCategoryController : Controller
    {
        private readonly IMediator _mediator;

        public ProductCategoryController(IMediator mediator)
        {
            _mediator = mediator;
        }
        public IActionResult Index()
        {
            var categories = _mediator.Send(new ProductCategoryListQuery());            
            return View(categories.Result);
        }

        public IActionResult Add()
        {
            return View(new ProductCategoryCreateViewModel());
        }


        [HttpPost]
        public async Task<IActionResult> Add(ProductCategoryCreateViewModel viewModel)
        {            
            var result = await _mediator.Send(new ProductCategoryCreateCommand { Name = viewModel.Name });
            return RedirectToAction("Detail", new { id = result });
        }

        public IActionResult Edit(int id)
        {
            _mediator.Publish(new OrderPlacedNotification());

            var category = _mediator.Send(new ProductCategoryDetailQuery { Id = id }).Result;
            return View(new ProductCategoryEditViewModel
            {
                Id = category.Id,
                Name = category.Name,
                Active = category.Active
            });
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ProductCategoryEditViewModel viewModel)
        {
            var category = await _mediator.Send(new ProductCategoryUpdateCommand
            {
                Id = viewModel.Id,
                Name = viewModel.Name,
                Active = viewModel.Active
            });

            return RedirectToAction("Detail", new { id = viewModel.Id });
        }

        public async Task<IActionResult> Detail(int id)
        {
            var detailModel = await _mediator.Send(new ProductCategoryDetailQuery { Id = id });
            return View(detailModel);
        }
    }
}