﻿using System.ComponentModel.DataAnnotations;

namespace eCommerce.Models
{
    public class ProductCategoryCreateViewModel
    {
        [Required]
        public string Name { get; set; }

        public bool Active { get; set; }
    }
}
