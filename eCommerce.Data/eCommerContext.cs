﻿using eCommerce.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace eCommerce.Data
{
    public class eCommerContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=(local);Initial Catalog=eCommerce;persist security info=True; Integrated security=SSPI;");
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCategory> ProductCategory { get; set; }
    }
}
