﻿using eCommerce.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace eCommerce.Data.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly DbContext _dbContext;

        public ProductRepository()
        {
            _dbContext = new eCommerContext();
        }

        public void Commit()
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Product> GetAll()
        {
            throw new NotImplementedException();
        }

        public Product GetById(int id)
        {
            throw new NotImplementedException();
        }

        public Guid Insert(Product product)
        {
            _dbContext.Add(product);
            return product.Id;
        }

        public void Update(Product product)
        {
            throw new NotImplementedException();
        }
    }
}
