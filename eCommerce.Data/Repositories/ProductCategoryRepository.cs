﻿using eCommerce.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace eCommerce.Data.Repositories
{
    public class ProductCategoryRepository : IProductCategoryRepository
    {
        private DbContext _dbContext = null;

        public ProductCategoryRepository()
        {
            _dbContext = new eCommerContext();
        }

        public void Commit()
        {
            _dbContext.SaveChanges();            
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProductCategory> GetAll()
        {
            return _dbContext.Set<ProductCategory>().ToList();
        }

        public ProductCategory GetById(int id)
        {
            return _dbContext.Set<ProductCategory>().Find(id);
        }

        public int Insert(ProductCategory productCategory)
        {
            _dbContext.Add<ProductCategory>(productCategory);
            return productCategory.Id;
        }

        public void Update(ProductCategory productCategory)
        {
            _dbContext.Entry(productCategory).State = EntityState.Modified;
        }
    }
}
