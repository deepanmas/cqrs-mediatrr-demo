﻿using eCommerce.Domain.Models;
using System.Collections.Generic;

namespace eCommerce.Data.Repositories
{
    public interface IProductCategoryRepository
    {
        IEnumerable<ProductCategory> GetAll();
        ProductCategory GetById(int id);
        int Insert(ProductCategory productCategory);
        void Update(ProductCategory productCategory);
        void Delete(int id);
        void Commit();
    }
}
