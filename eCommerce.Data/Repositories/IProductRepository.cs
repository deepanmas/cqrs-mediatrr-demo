﻿using eCommerce.Domain.Models;
using System;
using System.Collections.Generic;

namespace eCommerce.Data.Repositories
{
    public interface IProductRepository
    {
        IEnumerable<Product> GetAll();
        Product GetById(int id);
        Guid Insert(Product product);
        void Update(Product product);
        void Delete(int id);
        void Commit();
    }
}
