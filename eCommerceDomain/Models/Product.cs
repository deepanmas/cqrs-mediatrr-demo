﻿using System;

namespace eCommerce.Domain.Models
{
    public class Product        
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual ProductCategory ProductCategory { get; set; }
        public int ProductCategoryId { get; set; }
    }
}
