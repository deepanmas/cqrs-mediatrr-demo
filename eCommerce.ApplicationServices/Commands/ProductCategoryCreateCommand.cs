﻿using MediatR;

namespace eCommerce.ApplicationServices.Commands
{
    public class ProductCategoryCreateCommand: IRequest<int>
    {
        public string Name { get; set; }
    }
}
