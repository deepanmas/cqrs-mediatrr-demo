﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace eCommerce.ApplicationServices.Commands
{
    public class ProductCategoryUpdateCommand : IRequest<bool>
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool Active { get; set; }
    }
}
