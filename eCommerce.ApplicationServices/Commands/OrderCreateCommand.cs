﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace eCommerce.ApplicationServices.Commands
{
    public class OrderCreateCommand : IRequest<Guid>
    {
        public Guid Id { get; set; }
        public string OrderNumber { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
    }
}
