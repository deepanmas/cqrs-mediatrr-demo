﻿using eCommerce.ApplicationServices.Dto;
using MediatR;
using System.Collections.Generic;

namespace eCommerce.ApplicationServices.Queries
{
    public class ProductCategoryListQuery : IRequest<IEnumerable<ProductCategoryDetailModel>>
    {
        public bool Active { get; set; }
    }
}
