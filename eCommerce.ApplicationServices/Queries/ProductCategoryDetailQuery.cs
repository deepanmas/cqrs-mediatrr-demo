﻿using eCommerce.ApplicationServices.Dto;
using MediatR;

namespace eCommerce.ApplicationServices.Queries
{
    public class ProductCategoryDetailQuery : IRequest<ProductCategoryDetailModel>
    {
        public int Id { get; set; }
    }
}
