﻿using AutoMapper;
using eCommerce.ApplicationServices.Commands;
using eCommerce.ApplicationServices.Dto;
using eCommerce.ApplicationServices.Queries;
using eCommerce.Data.Repositories;
using eCommerce.Domain.Models;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace eCommerce.ApplicationServices.Handlers
{
    public class ProductCategoryHandler : IRequestHandler<ProductCategoryCreateCommand, int>,
        IRequestHandler<ProductCategoryUpdateCommand, bool>,
        IRequestHandler<ProductCategoryListQuery, IEnumerable<ProductCategoryDetailModel>>,
        IRequestHandler<ProductCategoryDetailQuery, ProductCategoryDetailModel>
    {
        IProductCategoryRepository _productRepository;
        IMapper _mapper;

        public ProductCategoryHandler(IMapper mapper)
        {
            _productRepository = new ProductCategoryRepository();
            _mapper = mapper;
        }

        public Task<int> Handle(ProductCategoryCreateCommand request, CancellationToken cancellationToken)
        {
            var categoryId = _productRepository.Insert(new ProductCategory { Name = request.Name });
            _productRepository.Commit();

            return Task.FromResult(categoryId);
        }

        public Task<bool> Handle(ProductCategoryUpdateCommand request, CancellationToken cancellationToken)
        {
            var category = _productRepository.GetById(request.Id);
            category.Name = request.Name;
            category.Active = request.Active;
            _productRepository.Update(category);

            _productRepository.Commit();

            return Task.FromResult(true);
        }

        public Task<IEnumerable<ProductCategoryDetailModel>> Handle(ProductCategoryListQuery request, CancellationToken cancellationToken)
        {
            var categories = _productRepository.GetAll();

            return Task.FromResult(_mapper.Map<IEnumerable<ProductCategoryDetailModel>>(categories));
        }

        public Task<ProductCategoryDetailModel> Handle(ProductCategoryDetailQuery request, CancellationToken cancellationToken)
        {
            var category = _productRepository.GetById(request.Id);
            return Task.FromResult(_mapper.Map<ProductCategoryDetailModel>(category));
        }
    }    
}
