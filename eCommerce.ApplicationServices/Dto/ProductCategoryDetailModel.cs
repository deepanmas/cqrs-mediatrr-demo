﻿namespace eCommerce.ApplicationServices.Dto
{
    public class ProductCategoryDetailModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
    }
}
