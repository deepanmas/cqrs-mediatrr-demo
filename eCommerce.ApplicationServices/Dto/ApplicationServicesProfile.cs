﻿using AutoMapper;
using eCommerce.Domain.Models;
using System.Collections.Generic;

namespace eCommerce.ApplicationServices.Dto
{
    public class ApplicationServicesProfile : Profile
    {
        public ApplicationServicesProfile()
        {
            //CreateMap<Product, ProductListModel>();
            //CreateMap<Product, IEnumerable<ProductListModel>>();
            //CreateMap<Product, ProductDetailModel>();
            CreateMap<ProductCategory, ProductCategoryDetailModel>();
            CreateMap<ProductCategory, IEnumerable<ProductCategoryDetailModel>>();
        }
    }
}
